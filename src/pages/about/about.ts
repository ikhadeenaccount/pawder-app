import {Component, Injectable} from '@angular/core';

import { NavController } from 'ionic-angular';

import {RestService} from "../../services/service.rest";
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import {InAppBrowser} from 'ionic-native';
import { SafariViewController } from 'ionic-native';

import {StorageService} from "../../services/service.db";
import {URLSearchParams} from "@angular/http";


@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
  providers: [ RestService ],
})

@NgModule({
  imports: [ BrowserModule, FormsModule ],
})

@Injectable()
export class AboutPage {

  posts = [];
  tags = "";
  last = null;
  thumb = null;
  tagsOfPost = "";
  hidden = true;

  constructor(public navCtrl: NavController,
              public restService: RestService,
              public storageService: StorageService) {

    if(!this.storageService.getData('sfw')){
      this.storageService.setData('sfw',false);
    }
    if(!this.storageService.getData('thumb')){
      this.storageService.setData('thumb', false);
    }

    //get the 5 most recent e621/e926 images
    this.storageService.getData('sfw').then(data => {
      //Check if user enabled SFW mode
      let url = 'https://e621.net/post/index.json?';
      if (data == true){
         url = 'https://e926.net/post/index.json?';
      }

      //set the parameters
      let params = new URLSearchParams();
      params.set('limit', '5');

      //get request and push last id
      this.restService.createGet(url, params).subscribe(data =>{

        for(let dat of data){
          dat.hidden = true;
          this.posts.push(dat);
        }

        // this.posts = data;
        this.last = data[data.length-1];
      });
    });

    //check whether user prefers thumbnails or full images
    this.storageService.getData('thumb').then(data =>{
      this.thumb = data;
    })

  }

  //Refresh object to get the latest images again
  doRefresh(refresher) {

    console.log('Begin async operation', refresher);

    setTimeout(() => {

      //get the 5 most recent e621/e926 images
      this.storageService.getData('sfw').then(data => {
        this.posts = [];

        //Check if user enabled SFW mode
        let url = 'https://e621.net/post/index.json?';
        if (data == true){
          url = 'https://e926.net/post/index.json?';
        }

        //set the parameters
        let params = new URLSearchParams();
        params.set('limit', '5');
        params.set('tags', this.tags);

        //get request and push last id
        this.restService.createGet(url, params).subscribe(data =>{

          for(let dat of data){

            dat.hidden = true;
            this.posts.push(dat);
          }

          // this.posts = data;
          this.last = data[data.length-1];

        });
      });

      //check whether user prefers thumbnails or full images
      this.storageService.getData('thumb').then(data =>{
        this.thumb = data;
      });

      console.log('Async operation has ended');
      refresher.complete();
    },500);

    }

  //When the user scrolls to the bottom of the page, it loads InfiniteScroll
  //This will get the next 5 pictures based on the ID of the last post in the for loop
  doInfinite(infiniteScroll) {

    setTimeout(() => {
      console.log('Begin async operation');

      this.storageService.getData('sfw').then(data => {

        //Check if user enabled SFW mode
        let url = 'https://e621.net/post/index.json?';
        if (data == true){
          url = 'https://e926.net/post/index.json?';
        }

        //Set the parameters
        let params = new URLSearchParams();
        params.set('limit', '5');
        params.set('tags', this.tags);
        params.set('before_id', this.last.id);

        //Get the response and update the last id
        this.restService.createGet(url, params).subscribe(data => {

          for(let dat of data){

            dat.hidden = true;
            this.posts.push(dat);
          }
          //Push can only take one item at a time
          //This parses every new image and adds it to the list instead of recreating it
          // for(let dat of data){
          //   this.posts.push(dat);
          // }

          this.last = data[data.length-1];

        });
      });

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

  //When user searches in searchbox, call this method
  searchTags(tags: any, limit: string, before_id: string) {

    this.storageService.getData('sfw').then(data => {

      this.posts = [];

      //Check if user enabled SFW mode
      let url = 'https://e621.net/post/index.json?';
      if (data == true){
        url = 'https://e926.net/post/index.json?';
      }

      //Set the parameters
      let params = new URLSearchParams();
      params.set('limit', '5');
      params.set('tags', tags.target.value);
      this.tags = tags.target.value;

      //Get the response and update the last id
      this.restService.createGet(url, params).subscribe(data => {
        for(let dat of data){
          dat.hidden = true;
          this.posts.push(dat);
        }
        // this.posts = data;
        this.last = data[data.length-1];

      });

    });

  }

  //whenother
  otherTags(tags: any, limit: string, before_id: string) {

    this.storageService.getData('sfw').then(data => {

      this.posts = [];

      //Check if user enabled SFW mode
      let url = 'https://e621.net/post/index.json?';
      if (data == true){
        url = 'https://e926.net/post/index.json?';
      }

      //Set the parameters
      let params = new URLSearchParams();
      params.set('limit', '5');
      params.set('tags', tags);
      this.tags = tags;

      //Get the response and update the last id
      this.restService.createGet(url, params).subscribe(data => {


        for(let dat of data){
          dat.hidden = true;
          this.posts.push(dat);
        }
        // this.posts = data;
        this.last = data[data.length-1];

      });

    });

  }

  getTags(post: any) {
    this.storageService.getData('sfw').then(data => {
      //Check if user enabled SFW mode
      let url = 'https://e621.net/post/tags.json?';
      if (data == true) {
        url = 'https://e926.net/post/tags.json?';
      }

      let params = new URLSearchParams();

      params.set('id', post.id);

      this.restService.createGet(url, params).subscribe(data => {
        console.log('TAGS:' + data);
        post.tagsOfPost = data;
        post.hidden = false;
      });
    });
  }


  //Show the full image in Chrome Custom Tabs and Safari Custom Tabs
  //If none of those are available (old android/iOS or Windows Phone)
  //it will use the angular in app browser instead
  showFullImg(post: string){

    //Load Chrome Custom Tabs if available
    SafariViewController.connectToService();

    SafariViewController.isAvailable()
      .then(
        (available: boolean) => {
          if(available){

            SafariViewController.show({
              url: post,
              hidden: false,
              animated: false,
              transition: 'curl',
              enterReaderModeIfAvailable: true,
              tintColor: '#ff0000'
            })
              .then(
                (result: any) => {
                  if(result.event === 'opened') console.log('Opened');
                  else if(result.event === 'loaded') console.log('Loaded');
                  else if(result.event === 'closed') console.log('Closed');
                },
                (error: any) => console.error(error)
              );

          } else {
            //use the Angular in app browser
            new InAppBrowser(post, '_blank');
          }
        }
      );
  }


}
