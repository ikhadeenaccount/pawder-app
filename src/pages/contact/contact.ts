import {Component, Injectable} from '@angular/core';

import { NavController } from 'ionic-angular';
import {StorageService} from "../../services/service.db";



@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'})

@Injectable()
export class ContactPage {

  thumb;
  sfw;
  constructor(public navCtrl: NavController,
              public storageService: StorageService) {
    this.storageService.getData('thumb').then(data =>{
      this.thumb = data;
    });
    this.storageService.getData('sfw').then(data =>{
      this.sfw = data;
    });
  }

  OnChange(preferenceType: string, data: any) {

    this.storageService.setData(preferenceType, data);

  }


}
