import {Injectable}     from '@angular/core';
import {Jsonp, URLSearchParams} from '@angular/http';
// Import RxJs required methods
// https://scotch.io/tutorials/angular-2-http-requests-with-observables
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from "rxjs";


@Injectable()
export class RestService {


  constructor(private jsonp: Jsonp) {

    console.log('Rest service initialized');

  }

  createGet(url: string,  params?: URLSearchParams): Observable<any>{

    params.set('callback', 'JSONP_CALLBACK');

    return this.jsonp
      .get(url, {search: params})
      .map(response => response.json())
      .catch((error: any) => Observable.throw(error.json() || 'Server error'));
  }

}
