import {Injectable}     from '@angular/core';

// Import RxJs required methods
// https://scotch.io/tutorials/angular-2-http-requests-with-observables
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {NativeStorage} from "ionic-native";

@Injectable()
export class StorageService {

  constructor() {

    console.log('Storage service initialized');
  }

  /**
   * Save data on device
   *
   * @param key
   * @param data
   */
  setData(key: string, data: any): Promise<any> {

    console.log('Storing data for key ', key);

    return new Promise<any>((resolve, reject) => {

      NativeStorage.setItem(key, data)
        .then(
          () => {
            console.log('Data stored for key ', key);
            resolve();
          },
          error => {

            console.error('Error storing data', error);
            let storage = window.localStorage;
            storage.setItem(key, JSON.stringify(data));
            resolve();
          }
        );
    });
  }

  /**
   * Remove data from device
   *
   * @param key
   */
  removeData(key: string): Promise<any> {

    console.log('Removing data for key ', key);

    return new Promise<any>((resolve, reject) => {

      NativeStorage.remove(key)
        .then(
          () => {
            console.log('Data removed for key ', key);
            resolve();
          },
          error => {

            console.error('Error removing data', error);
            let storage = window.localStorage;
            storage.removeItem(key);
            resolve();
          }
        );
    });
  }

  /**
   * Retrieve data from device
   *
   * @param key
   */
  getData(key: string): Promise<any> {

    console.log('Retrieving data for key', key);

    return new Promise<any>((resolve, reject) => {

      NativeStorage.getItem(key)
        .then(
          token => {
            console.log('Data retrieved for key ', key);
            resolve(token);
          },
          error => {

            console.error('Error retrieving data', error);
            let storage = window.localStorage;
            let data = storage.getItem(key);
            if (data !== undefined && data !== null) {

              return resolve(JSON.parse(data));
            }

            reject();
          }
        )
    });
  }
}
